﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;

namespace Utility.Tools.Swager
{
    public static class Extensions
    {
        public static void AddSwager(this IServiceCollection services, string Title)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = Title, Version = "v1" });
            });
            services.AddSwaggerGenNewtonsoftSupport();
        }
        public static void ConfigureSwager(this IApplicationBuilder app, string Name)
        {
            app.UseSwagger(c =>
            {
                c.SerializeAsV2 = true;
                //var basePath = "/v1";
                //c.PreSerializeFilters.Add((swaggerDoc, httpReq) =>
                //{
                //    swaggerDoc.Servers = new List<OpenApiServer> { new OpenApiServer { Url = $"{httpReq.Scheme}://{httpReq.Host.Value}{basePath}" } };
                //});
            });

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", Name);
            }); 
            
        }
    }
}
