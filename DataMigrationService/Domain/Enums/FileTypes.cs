﻿using Dto;
using System.Collections.Generic;
using System.Text;
using Utility.Tools.Auth;

namespace Enums
{
    public enum FileTypes 
    {
        Image = 1,
        Sound = 2,
        Movie = 3,
        Document = 4
    }
}
