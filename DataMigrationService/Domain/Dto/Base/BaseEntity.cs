﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dto
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }
        public long CreatedAt { get; set; }
    }
}
