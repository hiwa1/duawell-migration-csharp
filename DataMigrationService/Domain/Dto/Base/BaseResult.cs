﻿
using System;
using System.Collections.Generic;

namespace Dto
{
    public class BaseApiResult
    {
        public bool Status { get; set; }
        public string Message { get; set; }
    }
    public class BaseApiPageResult : BaseApiResult
    {
        public int BlockSize { get; set; }
    }
    public class FixedIntDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class FixedIntResultDto : BaseApiResult
    {
        public List<FixedIntDto> Object { get; set; }
    }
    public class FixedGuidResultDto : BaseApiResult
    {
        public List<FixedGuidDto> Object { get; set; }
    }
    public class FixedGuidDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }


}
