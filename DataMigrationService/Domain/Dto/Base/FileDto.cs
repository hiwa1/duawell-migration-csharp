﻿using Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Dto
{
    public class FileDto
    {
        public Guid Id { get; set; }
        public string Location{ get; set; }
        public FileTypes FileType { get; set; }
    }
}
