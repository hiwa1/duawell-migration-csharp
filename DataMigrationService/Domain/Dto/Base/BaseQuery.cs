﻿using System;

namespace Dto
{
    public class BaseByGuidIdDto
    {
        public Guid Id { get; set; }
    }
    public class BaseByIntIdDto
    {
        public int Id { get; set; }
    }

    public class BaseByPageDto
    {
        public int PageNo { get; set; }
    }
    public class BaseByPageResultDto:BaseApiResult
    {
        public int BlockCount { get; set; }        
    }
    public class BaseByUserDto
    {
        public Guid UserId { get; set; }
    }
    public class NameDto
    {
        public string Name { get; set; }
    }
}
