﻿using MongoDB.Bson;
using System.ComponentModel;

namespace Entities
{
    public class sectors
    {
        [DisplayName("enum")]
        public int Enum { get; set; }
        public BsonDateTime createdAt { get; set; }
        public BsonDateTime updatedAt { get; set; }
        public string name { get; set; }

    }
}
