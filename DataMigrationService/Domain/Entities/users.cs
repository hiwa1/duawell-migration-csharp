﻿using MongoDB.Bson;

namespace Entities
{
    public class users
    {
        public string city { get; set; }
        public string dateOfBirth { get; set; }
        public string email { get; set; }
        public string fullname { get; set; }
        public bool isActive { get; set; }
        public bool isDeleted { get; set; }
        public string password { get; set; }

        public roles role { get; set; }
        public string telephone { get; set; }
        public string username { get; set; }
        public BsonDateTime createdAt { get; set; }
        public BsonDateTime updatedAt { get; set; }
        public BsonDateTime lastLoginDate { get; set; }
        public int allowance { get; set; }
        public sectors sector { get; set; }
        public int percentage { get; set; }

    }
}
