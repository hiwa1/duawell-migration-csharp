﻿using MongoDB.Bson;

namespace Entities
{
    public class reportcategories
    {
        public string name { get; set; }
        public BsonDateTime createdAt { get; set; }
        public BsonDateTime updatedAt { get; set; }

    }
}
