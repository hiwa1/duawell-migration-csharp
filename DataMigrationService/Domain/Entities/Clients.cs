﻿using MongoDB.Bson;
using System.Collections.Generic;
using System.ComponentModel;

namespace Entities
{
    public class Clients
    {
        public InitialReading initialReading { get; set; }
        public suspendService suspendService { get; set; }
        public changePricePerUnit changePricePerUnit { get; set; }
        public changePreviousBalance changePreviousBalance { get; set; }
        public changeReading changeReading { get; set; }
        
        public string id { get; set; }
        public string customerID { get; set; }
        public sectors sector { get; set; }
        public string fullName { get; set; }
        public string telephone { get; set; }
        public string clockNumber { get; set; }
        public BsonDateTime createdAt { get; set; }
        public BsonDateTime updatedAt { get; set; }

    }
    public class InitialReading
    {
        public string startingReading { get; set; }
        public int initialReading { get; set; }

        public bool isDisabled { get; set; }
    }

    public class suspendService
    {
        public string startingReading { get; set; }
        public bool isDisabled { get; set; }
    }
    public class changePricePerUnit
    {
        public string pricePerUnit { get; set; }
        public bool isDisabled { get; set; }
    }
    public class changePreviousBalance
    {
        public string selectedMonth { get; set; }
        public bool isDisabled { get; set; }
        public string changeValue { get; set; }
    }
    public class changeReading
    {
        public string isDisabled { get; set; }
        public BsonDateTime selectedMonth { get; set; }
        public double previousValue { get; set; }
        public double newValue { get; set; }
        public double recentValue { get; set; }
        public double previousBalance { get; set; }
        public BsonDateTime readingMonth { get; set; }
        public double balance { get; set; }
        public string report { get; set; }
        public double credit { get; set; }

    }
}
