﻿using MongoDB.Bson;

namespace Entities
{
    public class staffs
    {
        public string className { get; set; }
        public string name { get; set; }
        public BsonDateTime birthDate { get; set; }
        public string telephone { get; set; }
        public string jobTitle { get; set; }
        public BsonDateTime startingDate { get; set; }
        public int salary { get; set; }
        public int allowance { get; set; }
        public string monthYear { get; set; }

    }
}
