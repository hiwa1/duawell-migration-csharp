﻿using MongoDB.Bson;
using System.Collections.Generic;
using System.ComponentModel;

namespace Entities
{
    public class Client
    {
        public string className { get; set; }
        public BsonDateTime created { get; set; }
        public string id { get; set; }
        public propertiesValues propertiesValues { get; set; }
        public List<service> service { get; set; }

    }

    public class propertiesValues 
    {
        [DisplayName("suspendservice-stop")]
        public string Telephone { get; set; }
        public string suspendserviceStop { get; set; }

        [DisplayName("Customer ID")]
        public string CustomerID { get; set; }
        public string Sector { get; set; }
        public string prefix { get; set; }
        public string price { get; set; }
        public string unsuspend { get; set; }
        public string id { get; set; }
        
        [DisplayName("Clock number")]
        public string ClockNumber { get; set; }
        
        [DisplayName("Full name")]
        public string FullName { get; set; }

    }
}
