﻿using MongoDB.Bson;

namespace Entities
{
    public class service
    {
        public string className { get; set; }
        public Client client { get; set; }
        public BsonDateTime date_checked { get; set; }
        public BsonDateTime date_paid { get; set; }
        public string newValue { get; set; }
        public int nextSuspendOrder { get; set; }
        public int overridePrevBalance { get; set; }
        public string paid { get; set; }
        public int previousSuspendOrder { get; set; }
        public string previousValue { get; set; }
        public bool suspended { get; set; }

    }
}
