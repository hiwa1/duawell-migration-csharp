﻿using MongoDB.Bson;

namespace Entities
{
    public class expenditure
    {
        public string className { get; set; }
        public BsonDateTime date { get; set; }
        public string type { get; set; }
        public string categoryName { get; set; }
        public string description { get; set; }
        public string receiver { get; set; }
        public string comment { get; set; }
        public double amount { get; set; }
        public double price { get; set; }
        public bool overridePrice { get; set; }

    }
}
