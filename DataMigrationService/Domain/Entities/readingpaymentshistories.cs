﻿using MongoDB.Bson;

namespace Entities
{
    public class readingpaymentshistories
    {
        public string MyProperty { get; set; }
        public string discount { get; set; }
        public string paidPreviously { get; set; }
        public Client client { get; set; }
        public BsonDateTime createdAt { get; set; }
        public BsonDateTime updatedAt { get; set; }

    }
}
