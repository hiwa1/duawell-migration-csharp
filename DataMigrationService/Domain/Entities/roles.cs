﻿using MongoDB.Bson;

namespace Entities
{
    public class roles
    {
        public string className { get; set; }
        public BsonDateTime firstUse { get; set; }
        public string primaryServiceName { get; set; }
        public int timesLoggedIn { get; set; }

    }
}
