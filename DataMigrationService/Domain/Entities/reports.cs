﻿using MongoDB.Bson;
using System.Collections.Generic;
using System.ComponentModel;

namespace Entities
{
    public class reports
    {
        public string name { get; set; }
        public reportcategories reportCategory { get; set; }
        public string title { get; set; }
        public int paperOrientation { get; set; }
        public string[] fields { get; set; }
        public List<Filters> filters { get; set; }
        public BsonDateTime firstUse { get; set; }

    }


    public class Filters
    {
        public string title { get; set; }
        public string id { get; set; }
        [DisplayName("checked")]
        public bool Checked { get; set; }
        public int filter { get; set; }
        public string keyword { get; set; }
    }
}
