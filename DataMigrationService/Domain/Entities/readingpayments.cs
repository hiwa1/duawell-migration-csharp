﻿using MongoDB.Bson;

namespace Entities
{
    public class readingpayments
    {
        public string previousReading { get; set; }
        public string recentReading { get; set; }
        public string difference { get; set; }
        public string pricePerUnit { get; set; }
        public string invoice { get; set; }
        public string previousBalance { get; set; }
        public string total { get; set; }
        public string paid { get; set; }
        public string balance { get; set; }
        public BsonDateTime datePaid { get; set; }
        public string discount { get; set; }
        public string paidPreviously { get; set; }
        public string credit { get; set; }
        public Client client { get; set; }
        public BsonDateTime createdAt { get; set; }
        public BsonDateTime updatedAt { get; set; }

    }
}
