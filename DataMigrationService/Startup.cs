﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Utility.Tools;
using Utility.Tools.Auth;
using Utility.Tools.Exceptions;
using Utility.Tools.Notification;
using Utility.Tools.SMS.Rahyab;
using Utility.Tools.Swager;
using Utility.Tools.Cors;
using Utility.General;
using Utility.Tools.Mongo;
using Contracts;
using Entities;
using Utility.Tools.General;

namespace EndPoint
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        [Obsolete]
        public void ConfigureServices(IServiceCollection services)
        {
            Configuration.GetSection<AdminSettings>();
            services.AddApplicationServices();
            services.AddRepositories();
            services.AddSwager("Migration");

            services.ConfigureCors("CorsPolicy");
           

            services.AddControllers(options =>
            {
                options.Filters.Add(typeof(CustomExceptionFilter));
                options.EnableEndpointRouting = false;
            }).AddSessionStateTempDataProvider().SetCompatibilityVersion(CompatibilityVersion.Version_2_1).AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                options.SerializerSettings.NullValueHandling = NullValueHandling.Include;
            });

            
            services.AddJwt(Configuration);
            services.AddScoped<IEncrypter, Encrypter>();
            services.AddScoped<IFireBase, FireBase>();
            services.AddMongo(Configuration);

            Seed(services);
        }

        private void Seed(IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();
            


        }

        [Obsolete]
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.ConfigureSwager("DataMigration");
            app.UseStaticFiles();
            app.UseCors("CorsPolicy");
            app.UseMvcWithDefaultRoute();
        }
    }
}
