﻿using Contracts;
using Dto;
using Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utility.Tools.General;

namespace ApplicationServices
{
    public class DoMigration : IDoMigration
    {
        private readonly IClientRepository clients_src;
        private readonly IExpenditureRepository expenditure;
        private readonly IReadingPaymentsRepository readingPayments;
        private readonly IreadingpaymentshistoriesRepository ireadingpaymentshistories;
        private readonly IreportcategoriesRepository ireportcategories;
        private readonly IreportsRepository ireports;
        private readonly IrolesRepository iroles;
        private readonly IsectorsRepository isectors;
        private readonly IserviceRepository iservice;
        private readonly IstaffsRepository istaffs;
        private readonly IusersRepository iusers;
        private readonly IClientsRepository clients_Dest;

        public DoMigration(IClientRepository clients,IExpenditureRepository expenditure,
            IReadingPaymentsRepository readingPayments,IreadingpaymentshistoriesRepository ireadingpaymentshistories,
            IreportcategoriesRepository ireportcategories,IreportsRepository ireports, IrolesRepository iroles,IsectorsRepository isectors,
            IserviceRepository iservice,IstaffsRepository istaffs,IusersRepository iusers,IClientsRepository clients_dest)
        {
            this.clients_src = clients;
            this.expenditure=expenditure;
            this.readingPayments=readingPayments;
            this.ireadingpaymentshistories=ireadingpaymentshistories;
            this.ireportcategories=ireportcategories;
            this.ireports=ireports;
            this.iroles=iroles;
            this.isectors=isectors;
            this.iservice=iservice;
            this.istaffs=istaffs;
            this.iusers=iusers;
            clients_Dest=clients_dest;
        }
        public string Execute()
        {
            var allClients = clients_src.GetAll();
            allClients.ForEach(client => 
            {
                var newClient = new Clients
                {
                    initialReading = new InitialReading {

                },
                suspendService = new suspendService
                {
                },
                changeReading = new changeReading
                {
                },
                changePreviousBalance = new changePreviousBalance
                {
                },
                changePricePerUnit = new changePricePerUnit
                {
                },
                };
                clients_Dest.Insert(newClient);
            });
            return "OK";
        }
    }
}
