﻿using Dto;
using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationServices
{
    public interface IAddClient : IApplicationService
    {
        string Execute(Client dto);
    }
}
