﻿using Contracts;
using Dto;
using Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Utility.Tools.General;

namespace ApplicationServices
{
    public class AddClient : IAddClient
    {
        private readonly IClientRepository clients;

        public AddClient(IClientRepository clients)
        {
            this.clients = clients;
        }
        public string Execute(Client dto)
        {
            clients.Insert(dto);
            return "OK";
        }
    }
}
