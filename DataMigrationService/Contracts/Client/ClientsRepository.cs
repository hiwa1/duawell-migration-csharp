﻿using Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Contracts;

namespace Contracts
{
    public class ClientsRepository : IClientsRepository, IRepository
    {
        private readonly IMongoDatabase database;

        public ClientsRepository(IMongoDatabase database)            
        {
            this.database = database;
        }
        private IMongoCollection<Clients> clients => database.GetCollection<Clients>("Clients");

        public List<Clients> GetAll()
        {
            return clients.Find(p=>true).ToList();
        }

        public long GetCount()
        {
            return clients.CountDocuments(p=>true);
        }

        public void Insert(Clients client)
        {
            clients.InsertOne(client);
        }
    }
}
