﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IClientsRepository
    {
        void Insert(Clients cert);
        List<Clients> GetAll();
        long GetCount();
    }
}
