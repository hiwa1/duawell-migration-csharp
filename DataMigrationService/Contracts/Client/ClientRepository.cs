﻿using Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Contracts;

namespace Contracts
{
    public class ClientRepository : IClientRepository, IRepository
    {
        private readonly IMongoDatabase database;

        public ClientRepository(IMongoDatabase database)            
        {
            this.database = database;
        }
        private IMongoCollection<Client> clients => database.GetCollection<Client>("Client");

        public List<Client> GetAll()
        {
            return clients.Find(p=>true).ToList();
        }

        public long GetCount()
        {
            return clients.CountDocuments(p=>true);
        }

        public void Insert(Client client)
        {
            clients.InsertOne(client);
        }
    }
}
