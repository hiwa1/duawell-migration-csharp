﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IClientRepository
    {
        void Insert(Client cert);
        List<Client> GetAll();
        long GetCount();
    }
}
