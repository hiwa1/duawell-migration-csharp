﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IreportcategoriesRepository
    {
        void Insert(reportcategories cert);
        List<reportcategories> GetAll();
        long GetCount();
    }
}
