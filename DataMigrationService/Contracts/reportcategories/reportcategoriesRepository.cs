﻿using Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Contracts;

namespace Contracts
{
    public class reportcategoriesRepository : IreportcategoriesRepository, IRepository
    {
        private readonly IMongoDatabase database;

        public reportcategoriesRepository(IMongoDatabase database)            
        {
            this.database = database;
        }
        private IMongoCollection<reportcategories> reportCat => database.GetCollection<reportcategories>("reportcategories");

        public List<reportcategories> GetAll()
        {
            return reportCat.Find(p=>true).ToList();
        }

        public long GetCount()
        {
            return reportCat.CountDocuments(p=>true);
        }

        public void Insert(reportcategories rc)
        {
            reportCat.InsertOne(rc);
        }
    }
}
