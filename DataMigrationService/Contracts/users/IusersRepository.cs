﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IusersRepository
    {
        void Insert(users cert);
        List<users> GetAll();
        long GetCount();
    }
}
