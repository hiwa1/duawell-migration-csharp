﻿using Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Contracts;

namespace Contracts
{
    public class usersRepository : IusersRepository, IRepository
    {
        private readonly IMongoDatabase database;

        public usersRepository(IMongoDatabase database)            
        {
            this.database = database;
        }
        private IMongoCollection<users> users => database.GetCollection<users>("users");

        public List<users> GetAll()
        {
            return users.Find(p=>true).ToList();
        }

        public long GetCount()
        {
            return users.CountDocuments(p=>true);
        }

        public void Insert(users u)
        {
            users.InsertOne(u);
        }
    }
}
