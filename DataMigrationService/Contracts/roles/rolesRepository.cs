﻿using Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Contracts;

namespace Contracts
{
    public class rolesRepository : IrolesRepository, IRepository
    {
        private readonly IMongoDatabase database;

        public rolesRepository(IMongoDatabase database)            
        {
            this.database = database;
        }
        private IMongoCollection<roles> roles => database.GetCollection<roles>("Client");

        public List<roles> GetAll()
        {
            return roles.Find(p=>true).ToList();
        }

        public long GetCount()
        {
            return roles.CountDocuments(p=>true);
        }

        public void Insert(roles client)
        {
            roles.InsertOne(client);
        }
    }
}
