﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IrolesRepository
    {
        void Insert(roles cert);
        List<roles> GetAll();
        long GetCount();
    }
}
