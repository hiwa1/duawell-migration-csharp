﻿using Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Contracts;

namespace Contracts
{
    public class reportsRepository : IreportsRepository, IRepository
    {
        private readonly IMongoDatabase database;

        public reportsRepository(IMongoDatabase database)            
        {
            this.database = database;
        }
        private IMongoCollection<reports> reports => database.GetCollection<reports>("reports");

        public List<reports> GetAll()
        {
            return reports.Find(p=>true).ToList();
        }

        public long GetCount()
        {
            return reports.CountDocuments(p=>true);
        }

        public void Insert(reports rep)
        {
            reports.InsertOne(rep);
        }
    }
}
