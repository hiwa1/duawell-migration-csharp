﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IreportsRepository
    {
        void Insert(reports cert);
        List<reports> GetAll();
        long GetCount();
    }
}
