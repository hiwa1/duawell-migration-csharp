﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IstaffsRepository
    {
        void Insert(staffs cert);
        List<staffs> GetAll();
        long GetCount();
    }
}
