﻿using Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Contracts;

namespace Contracts
{
    public class staffsRepository : IstaffsRepository, IRepository
    {
        private readonly IMongoDatabase database;

        public staffsRepository(IMongoDatabase database)            
        {
            this.database = database;
        }
        private IMongoCollection<staffs> staffs => database.GetCollection<staffs>("staffs");

        public List<staffs> GetAll()
        {
            return staffs.Find(p=>true).ToList();
        }

        public long GetCount()
        {
            return staffs.CountDocuments(p=>true);
        }

        public void Insert(staffs sta)
        {
            staffs.InsertOne(sta);
        }
    }
}
