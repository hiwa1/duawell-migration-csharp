﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IExpenditureRepository
    {
        void Insert(expenditure cert);
        List<expenditure> GetAll();
        long GetCount();
    }
}
