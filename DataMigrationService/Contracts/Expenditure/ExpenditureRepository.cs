﻿using Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Contracts;

namespace Contracts
{
    public class ExpenditureRepository : IExpenditureRepository, IRepository
    {
        private readonly IMongoDatabase database;

        public ExpenditureRepository(IMongoDatabase database)            
        {
            this.database = database;
        }
        private IMongoCollection<expenditure> expen => database.GetCollection<expenditure>("expenditure");

        public List<expenditure> GetAll()
        {
            return expen.Find(p=>true).ToList();
        }

        public long GetCount()
        {
            return expen.CountDocuments(p=>true);
        }

        public void Insert(expenditure ex)
        {
            expen.InsertOne(ex);
        }
    }
}
