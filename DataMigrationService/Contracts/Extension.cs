﻿using ApplicationServices;
using Contracts;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Contracts
{
    public static class Extension
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            var applicationServiceType = typeof(IApplicationService).Assembly;
            var AllApplicationServices = applicationServiceType.ExportedTypes
               .Where(x => x.IsClass && x.IsPublic && x.FullName.Contains("ApplicationServices")).ToList();
            foreach (var type in AllApplicationServices)
            {
                Console.WriteLine(type.Name);
                services.AddScoped(type.GetInterface($"I{type.Name}"), type);
            }
        }
        public static void AddRepositories(this IServiceCollection services)
        {
            var mongoRepositpryType = typeof(IRepository).Assembly;
            var AllMongoRepositories = mongoRepositpryType.ExportedTypes
               .Where(x => !x.Name.StartsWith("IRepository") && x.Name.Contains("Repository") && x.IsClass && x.IsPublic)
               .ToList();
            Console.WriteLine($"************* {AllMongoRepositories.Count} ************");
            foreach (var type in AllMongoRepositories)
            {
                //try
                //{
                Console.WriteLine(type.Name);
                services.AddScoped(type.GetInterface($"I{type.Name}"), type);
                //}
                //catch { }
            }
        }
    }
}
