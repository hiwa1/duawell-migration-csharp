﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IserviceRepository
    {
        void Insert(service cert);
        List<service> GetAll();
        long GetCount();
    }
}
