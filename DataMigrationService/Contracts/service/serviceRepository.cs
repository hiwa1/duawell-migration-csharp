﻿using Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Contracts;

namespace Contracts
{
    public class serviceRepository : IserviceRepository, IRepository
    {
        private readonly IMongoDatabase database;

        public serviceRepository(IMongoDatabase database)            
        {
            this.database = database;
        }
        private IMongoCollection<service> service => database.GetCollection<service>("service");

        public List<service> GetAll()
        {
            return service.Find(p=>true).ToList();
        }

        public long GetCount()
        {
            return service.CountDocuments(p=>true);
        }

        public void Insert(service ser)
        {
            service.InsertOne(ser);
        }
    }
}
