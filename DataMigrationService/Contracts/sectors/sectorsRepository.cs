﻿using Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Contracts;

namespace Contracts
{
    public class sectorsRepository : IsectorsRepository, IRepository
    {
        private readonly IMongoDatabase database;

        public sectorsRepository(IMongoDatabase database)            
        {
            this.database = database;
        }
        private IMongoCollection<sectors> sectors => database.GetCollection<sectors>("sectors");

        public List<sectors> GetAll()
        {
            return sectors.Find(p=>true).ToList();
        }

        public long GetCount()
        {
            return sectors.CountDocuments(p=>true);
        }

        public void Insert(sectors client)
        {
            sectors.InsertOne(client);
        }
    }
}
