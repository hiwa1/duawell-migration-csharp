﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IsectorsRepository
    {
        void Insert(sectors cert);
        List<sectors> GetAll();
        long GetCount();
    }
}
