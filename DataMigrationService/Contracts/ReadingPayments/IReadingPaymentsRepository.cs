﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IReadingPaymentsRepository
    {
        void Insert(readingpayments cert);
        List<readingpayments> GetAll();
        long GetCount();
    }
}
