﻿using Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Contracts;

namespace Contracts
{
    public class ReadingPaymentsRepository : IReadingPaymentsRepository, IRepository
    {
        private readonly IMongoDatabase database;

        public ReadingPaymentsRepository(IMongoDatabase database)            
        {
            this.database = database;
        }
        private IMongoCollection<readingpayments> readingPayments => database.GetCollection<readingpayments>("readingpayments");

        public List<readingpayments> GetAll()
        {
            return readingPayments.Find(p=>true).ToList();
        }

        public long GetCount()
        {
            return readingPayments.CountDocuments(p=>true);
        }

        public void Insert(readingpayments rp)
        {
            readingPayments.InsertOne(rp);
        }
    }
}
