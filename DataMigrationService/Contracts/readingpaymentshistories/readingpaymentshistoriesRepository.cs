﻿using Entities;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using Contracts;

namespace Contracts
{
    public class readingpaymentshistoriesRepository : IreadingpaymentshistoriesRepository, IRepository
    {
        private readonly IMongoDatabase database;

        public readingpaymentshistoriesRepository(IMongoDatabase database)            
        {
            this.database = database;
        }
        private IMongoCollection<readingpaymentshistories> paymentHistory => database.GetCollection<readingpaymentshistories>("readingpaymentshistories");

        public List<readingpaymentshistories> GetAll()
        {
            return paymentHistory.Find(p=>true).ToList();
        }

        public long GetCount()
        {
            return paymentHistory.CountDocuments(p=>true);
        }

        public void Insert(readingpaymentshistories rph)
        {
            paymentHistory.InsertOne(rph);
        }
    }
}
