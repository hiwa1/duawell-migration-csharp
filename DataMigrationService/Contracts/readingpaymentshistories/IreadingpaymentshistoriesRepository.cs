﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts
{
    public interface IreadingpaymentshistoriesRepository
    {
        void Insert(readingpaymentshistories cert);
        List<readingpaymentshistories> GetAll();
        long GetCount();
    }
}
