﻿using ApplicationServices;
using Dto;
using Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace Controllers
{
    public class ClientController : SimpleController
    {
        private readonly IAddClient addVisit;

        public ClientController(
            IAddClient addVisit
            )
        {
            this.addVisit = addVisit;
        }

        [HttpPost]
        public ActionResult<string> AddClient([FromBody] Client dto)
        {
            return addVisit.Execute(dto);
        }
       

    }
}
