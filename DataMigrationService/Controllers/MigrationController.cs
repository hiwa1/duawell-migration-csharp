﻿using ApplicationServices;
using Dto;
using Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utility.Tools.General;

namespace Controllers
{
    public class MigrationController : SimpleController
    {
        private readonly IDoMigration doMigration;

        public MigrationController(
            IDoMigration doMigration
            )
        {
            this.doMigration = doMigration;
        }

        [HttpPost]
        public ActionResult<string> StartMigration([FromBody] Client dto)
        {
            return doMigration.Execute();
        }
       

    }
}
