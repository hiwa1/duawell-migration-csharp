﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class SimpleController : ControllerBase
    {
    }
}
